files=$(patsubst %.c,%.o,$(wildcard *.c))
.PHONY=sandeep
sandeep:mall
	echo "cfiles $(wildcard *.c)"
	echo "my files =====$(files)====$(files_cpp)"
	echo "compilation completed"
mall : $(files) 

%.o:%.c
	gcc -c $? -o $@
.PHONY=clean
clean:
	-rm *.o
